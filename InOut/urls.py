from django.urls import path
from . import views

app_name = "in_out"

urlpatterns = [
    path('', views.pemasukan, name = 'pemasukan'),
    path('pengeluaran/', views.pengeluaran, name ='pengeluaran')
]