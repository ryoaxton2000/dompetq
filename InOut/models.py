from django.db import models
from django.core.validators import MaxValueValidator

# Create your models here.
class Saldo(models.Model):
    saldo = models.BigIntegerField(default = 0)

class Pengeluaran_Pemasukan(models.Model):
    tanggal = models.DateField(auto_now_add=True)
    jam = models.TimeField(auto_now_add=True)
    keterangan = models.CharField(default = "", max_length = 100)
    status = models.CharField(default = "",max_length = 10)
    nominal = models.IntegerField(default = 0, validators=[MaxValueValidator(9999999999)])
    saldo = models.BigIntegerField(default = 0)
