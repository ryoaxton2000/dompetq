from django.shortcuts import render
from . import forms
from . models import Pengeluaran_Pemasukan, Saldo
from django.shortcuts import redirect

# Create your views here.
def pemasukan(request):
    counter = 0
    for s in Saldo.objects.all() :
        counter = counter + 1
    if counter == 0:
        data = {'saldo':0}
        saldo = forms.Create_Saldo(data)
        if saldo.is_valid():
            saldo.save()

    saldo = Saldo.objects.all()[0]
    if request.method == 'POST':
        form = forms.Rincian(request.POST)
        if form.is_valid():
            objectRincian = form.save(commit=False)
            objectRincian.status = 'Pemasukan'
            objectRincian.saldo = saldo.saldo + objectRincian.nominal
            objectRincian.save()
            saldo.saldo =  objectRincian.saldo
            saldo.save()
            return redirect('in_out:pemasukan')
    else:
        form = forms.Rincian()
    dataDompet = Pengeluaran_Pemasukan.objects.all().order_by('-tanggal', '-jam')
    context = {
        'form':form,
        'dataDompet':dataDompet,
        'saldo':saldo
    }
    return render(request, 'pemasukan.html', context)

def pengeluaran(request):
    counter = 0
    for s in Saldo.objects.all() :
        counter = counter + 1
    if counter == 0:
        data = {'saldo':0}
        saldo = forms.Create_Saldo(data)
        if saldo.is_valid():
            saldo.save()

    saldo = Saldo.objects.all()[0]
    if request.method == 'POST':
        form = forms.Rincian(request.POST)
        if form.is_valid():
            objectRincian = form.save(commit=False)
            objectRincian.status = 'Pengeluaran'
            objectRincian.saldo = saldo.saldo - objectRincian.nominal
            objectRincian.save()
            saldo.saldo =  objectRincian.saldo
            saldo.save()
            return redirect('in_out:pemasukan')
    else:
        form = forms.Rincian()
    dataDompet = Pengeluaran_Pemasukan.objects.all().order_by('-tanggal', '-jam')
    context = {
        'form':form,
        'dataDompet':dataDompet,
        'saldo':saldo
    }
    return render(request, 'pengeluaran.html', context)

def getSaldo():
    counter = 0
    for s in Saldo.objects.all() :
        counter = counter + 1
    if counter == 0:
        data = {'saldo':0}
        saldo = forms.Create_Saldo(data)
        if saldo.is_valid():
            saldo.save()
    saldo = Saldo.objects.all()[0]
    return saldo
