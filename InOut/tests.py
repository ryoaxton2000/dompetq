from django.test import TestCase, Client
from .models import Saldo, Pengeluaran_Pemasukan
from datetime import datetime
from .forms import Rincian

# Create your tests here.

c = Client()

class Pemasukan_Test(TestCase) :
    def test_pemasukan_page(self):
        response = c.get("/riwayat/")
        self.assertEqual(response.status_code, 200)

    def test_ada_kata_sambutan(self):
        response = c.get("/riwayat/")
        case = "Masukan data pemasukan / pengeluaran kamu disini"
        content = response.content.decode("utf8")
        self.assertIn(case, content)

    def test_sedang_diPage_pemasukan(self):
        response = c.get("/riwayat/")
        content = response.content.decode("utf8")
        case =  '<p id="current">Pemasukan</p>'
        self.assertIn(case, content)

    def test_tombol_pengeluaran(self):
        response = c.get("/riwayat/")
        content = response.content.decode("utf8")
        case = '<a id="movePage"'
        self.assertIn(case, content)
    
    def test_button_submit(self):
        response = c.get("/riwayat/")
        content = response.content.decode("utf8")
        case = '<input class="submitButton" type="submit" value="Simpan">'
        self.assertIn(case, content)

    def test_ada_form(self):
        response = c.get("/riwayat/")
        content = response.content.decode("utf8")
        case1 = '<label>Catatan: '
        self.assertIn(case1, content)
        case2 = '<input type="text" name="keterangan"'
        self.assertIn(case2, content)
        case3 = '<label>Nominal: '
        self.assertIn(case3, content)
        case4 = '<input type="number" name="nominal"'
        self.assertIn(case4, content)

    def test_text_riwayat(self):
        response = c.get("/riwayat/")
        content = response.content.decode("utf8")
        self.assertIn('Riwayat', content)

    def test_tabel_data(self):
        response = c.get("/riwayat/")
        content = response.content.decode("utf8")
        self.assertIn('Tanggal', content)
        self.assertIn('Catatan', content)
        self.assertIn('Jenis', content)
        self.assertIn('Nominal', content)
        self.assertIn('Saldo', content)

    def test_input_form(self):
        catatan = 'test'
        nominal = 10000
        form_data={'keterangan':catatan, 'nominal':nominal}
        form = Rincian(data = form_data)
        self.assertTrue(form.is_valid())
        response1 = c.post("/riwayat/", data=form_data)
        response2 = c.get("/riwayat/")
        content = response2.content.decode("utf8")
        self.assertIn('<p class="tanggal">'+datetime.today().strftime("%b. %d, %Y"), content)
        self.assertIn('<p class="catatan">'+catatan, content)
        self.assertIn('<p class="jenis">Pemasukan', content)
        self.assertIn('<p class="nominal">Rp'+str(nominal), content)
        self.assertIn('<p class="saldo"', content)

    def test_overflow(self):
        catatan1 = 'test1'
        nominal1 = 9999999999
        form_data={'keterangan':catatan1, 'nominal':nominal1}
        response1 = c.post("/riwayat/", data=form_data)
        catatan2 ='test2'
        nominal2= 1000
        form_data2={'keterangan':catatan2, 'nominal':nominal2}
        response2 = c.post("/riwayat/", data=form_data2)
        response3 = c.get("/riwayat/")
        content = response3.content.decode("utf8")
        case = "Saldomu melewati batas yang dapat ditampung DompetQ :("
        self.assertIn(case, content)

class Pengeluaran_Test(TestCase):
    def test_pengeluaran_page(self):
        response = c.get("/riwayat/pengeluaran/")
        self.assertEqual(response.status_code, 200)
    
    def test_ada_kata_sambutan(self):
        response = c.get("/riwayat/pengeluaran/")
        case = "Masukan data pemasukan / pengeluaran kamu disini"
        content = response.content.decode("utf8")
        self.assertIn(case, content)

    def test_sedang_diPage_pemasukan(self):
        response = c.get("/riwayat/pengeluaran/")
        content = response.content.decode("utf8")
        case =  '<p id="current">Pengeluaran</p>'
        self.assertIn(case, content)

    def test_tombol_pengeluaran(self):
        response = c.get("/riwayat/pengeluaran/")
        content = response.content.decode("utf8")
        case = '<a id="movePage"'
        self.assertIn(case, content)
    
    def test_button_submit(self):
        response = c.get("/riwayat/pengeluaran/")
        content = response.content.decode("utf8")
        case = '<input class="submitButton" type="submit" value="Simpan">'
        self.assertIn(case, content)

    def test_ada_form(self):
        response = c.get("/riwayat/pengeluaran/")
        content = response.content.decode("utf8")
        case1 = '<label>Catatan: '
        self.assertIn(case1, content)
        case2 = '<input type="text" name="keterangan"'
        self.assertIn(case2, content)
        case3 = '<label>Nominal: '
        self.assertIn(case3, content)
        case4 = '<input type="number" name="nominal"'
        self.assertIn(case4, content)

    def test_text_riwayat(self):
        response = c.get("/riwayat/pengeluaran/")
        content = response.content.decode("utf8")
        self.assertIn('Riwayat', content)

    def test_tabel_data(self):
        response = c.get("/riwayat/pengeluaran/")
        content = response.content.decode("utf8")
        self.assertIn('Tanggal', content)
        self.assertIn('Catatan', content)
        self.assertIn('Jenis', content)
        self.assertIn('Nominal', content)
        self.assertIn('Saldo', content)

    def test_input_form(self):
        catatan = 'test'
        nominal = 10000
        form_data={'keterangan':catatan, 'nominal':nominal}
        form = Rincian(data = form_data)
        self.assertTrue(form.is_valid())
        response1 = c.post("/riwayat/pengeluaran/", data=form_data)
        response2 = c.get("/riwayat/pengeluaran/")
        content = response2.content.decode("utf8")
        self.assertIn('<p class="tanggal">'+datetime.today().strftime("%b. %d, %Y"), content)
        self.assertIn('<p class="catatan">'+catatan, content)
        self.assertIn('<p class="jenis">Pengeluaran', content)
        self.assertIn('<p class="nominal">Rp'+str(nominal), content)
        self.assertIn('<p class="saldo">Rp-', content)
        case = '<div id="Warning">'
        self.assertIn(case, content)

    def test_overflow(self):
        catatan1 = 'test1'
        nominal1 = 9999999999
        form_data={'keterangan':catatan1, 'nominal':nominal1}
        response1 = c.post("/riwayat/pengeluaran/", data=form_data)
        catatan2 ='test2'
        nominal2= 1000
        form_data2={'keterangan':catatan2, 'nominal':nominal2}
        response2 = c.post("/riwayat/pengeluaran/", data=form_data2)
        response3 = c.get("/riwayat/pengeluaran/")
        content = response3.content.decode("utf8")
        case = "Saldomu melewati batas yang dapat ditampung DompetQ :("
        self.assertIn(case, content)