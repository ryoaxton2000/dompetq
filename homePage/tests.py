from django.test import TestCase, Client

# Create your tests here.
c = Client()

class Home_Test(TestCase):
    def test_home(self):
        response = c.get("")
        self.assertEqual(response.status_code, 200)

    def test_sambutan(self):
        response = c.get("")
        content = response.content.decode("utf8")
        self.assertIn("Halo!", content)
        self.assertIn("Selamat datang di DompetQ", content)

    def test_fitur(self):
        response = c.get("")
        content = response.content.decode("utf8")
        self.assertIn("Pemasukan Pengeluaran & Riwayat", content)
        self.assertIn("Jadwal Pengeluaran", content)
        self.assertIn("Wishlist", content)
        self.assertIn("Daftar Promo", content)

    def test_about(self):
        response = c.get("")
        content = response.content.decode("utf8")
        self.assertIn('<div id="aboutUs"', content)
    
