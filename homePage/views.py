from django.shortcuts import render
import InOut

# Create your views here.
def home_page(request):
    saldo = InOut.views.getSaldo()
    return render(request, "home.html", {'saldo':saldo})