from django.test import TestCase
from django.urls import resolve
from . import views
from django.http import HttpRequest

# Create your tests here.

class PromoPageTest(TestCase):
    def test_promo_url_exist(self):
        #test url  '/' exist
        response = self.client.get('/promolist/')
        self.assertEqual(response.status_code, 200)
    
    def test_promolist_using_promolist_page(self):
        #test url '/' will using landing page template
        response = self.client.get('/promolist/')
        self.assertTemplateUsed(response, 'promolist.html')
    
    def test_promolist_calling_promolist_views_function(self):
        found = resolve('/promolist/')
        self.assertEqual(found.func, views.promoisi)
    
    

