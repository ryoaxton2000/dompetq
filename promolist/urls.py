from django.urls import path
from . import views

app_name = "promolist"

urlpatterns = [
    path('', views.promoisi, name = 'promolist'),
    path('create/', views.promoisi_create, name= 'create'),
    path('delete/', views.promoisi_delete, name ='delete'),
]