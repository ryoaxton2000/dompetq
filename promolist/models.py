from django.db import models
from datetime import datetime

# Create your models here.
class isipromo(models.Model):

    KATEGORI_CHOICES = (
        ('Makanan','Makanan'),
        ('Transportasi','Transportasi'),
        ('Rekreasi','Rekreasi'),
        ('Lain-Lain','Lain-Lain'),
    )
    Kategori = models.CharField(max_length=100, choices = KATEGORI_CHOICES)
    Nama_Promo = models.CharField(max_length=30)
    Kode_Promo = models.CharField(max_length=30)
    Mulai_Promo = models.DateTimeField(default=datetime.now)
    Promo_Sampai = models.DateTimeField( default=datetime.now)
    

    def __str__(self):
        return self.Kategori