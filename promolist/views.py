from django.shortcuts import render
from . import forms
from .models import isipromo
from django.shortcuts import redirect
# Create your views here.

def promoisi(request):
    promos = isipromo.objects.all().order_by()
    return render(request, 'promolist.html', {'promos': promos})

def promoisi_create(request):
    if request.method == 'POST':
        form = forms.PromoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('promolist:promolist')

    else:
        form = forms.PromoForm()
    return render(request, 'promo_create.html', {'form': form})

def promoisi_delete(request):
    isipromo.objects.all().delete()
    return render(request, "promolist.html")
