[![pipeline status](https://gitlab.com/ryoaxton2000/dompetq/badges/master/pipeline.svg)](https://gitlab.com/ryoaxton2000/dompetq/commits/master)


## Anggota kelompok :
- Astrid Chaerida 1806191370
- Alwan Harrits  Surya Ihsan 1806205483
- Syanne Limarwan 1806191282
- Ryo Axtonlie 1806205571

## Nama apilikasi :
DompetQ

## Link Herokuapp :
http://dompetq.herokuapp.com

## Deskripsi :
Tentang aplikasi ini :
Aplikasi yang kita buat bertujuan untuk memudahkan user mendata pengeluaran dan pemasukkan mereka. Aplikasi ini memiliki fitur-fitur yang dapat dimanfaatkan untuk mengelola keuangan user sehari-hari dan dapat menambahkan wishlist (sesuatu yang dibeli/sebuah pengeluaran) dengan harapan mereka dapat menimbang pengeluaran mereka. Aplikasi ini juga berniat agar user dapat menggunakan promo-promo yang ada agar dapat 

Aplikasi ini adalah sebuah platform yang membantu user untuk mengelola keuangannya.

## Fitur :
- In/Out :
Seorang user akan diminta untuk memasukkan daftar pemasukan dan pengeluarannya dalam waktu tertentu. User akan diminta dengan format catatan dan nominal yang kemudian akan kami tampilkan berupa history pemasukan dan pengeluaran berupa tabel.

- Wishlist :
Jika user ingin mencatat daftar barang yang ingin dibeli user dapat menggunakan fitur wishlist. Dalam menggunakan fitur wishlist user diminta untuk melakukan input berupa nama barang, harga barang, dan gambar barang yang diinginkan. Nantinya  wishlist tersebut akan ditampilkan berupa daftar.

- Jadwal :
Fitur ini akan memudahkan pengguna sebagai pengingat daftar kewajiban yang harus dibayar oleh pengguna pada waktu tertentu. User akan diminta untuk melakukan input berupa catatan, nominal, dan tanggal pembayaran yang harus dilakukan. User akan melihat daftar kewajiban yang harus dibayarkan berdasarkan tanggal. 

- Promolist :
Fitur ini dimaksudkan agar user dapat menginput promo-promo yang akan dapat dimanfaatkannya. Agar promo tercatat, user akan diminta untuk memberikan inputan keterangan promo dengan jelas seperti kategorinya, nama promo(misal: grab), kode promo, tanggal promo mulai dan tanggal berakhirnya promo. Setelah menambahkan promo user akan dapat melihat promo yang sedang ada di halaman Promolist.
