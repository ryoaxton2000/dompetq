from django.test import TestCase, Client
from .models import JadwalForm
from .forms import Jadwal

# Create your tests here.
class WelcomeTest(TestCase):
    def test_URL(self):
        c = Client()
        response = c.get('/jadwal/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_ada_table(self):
        c = Client()
        response = c.get('/jadwal/')
        content = response.content.decode('utf8')
        self.assertIn("<table", content)

    def test_apakah_ada_button(self):
        c = Client()
        response = c.get('/jadwal/')
        content = response.content.decode('utf8')
        self.assertIn("<button", content)

    def test_form(self):
        form_data = {
            'catatan': 'catatan',
            'nominal': '100000',
            'tanggal': '08/09/2019'
        }
        form = Jadwal(data=form_data)  
        self.assertTrue(form.is_valid())