from django.shortcuts import render,redirect
from InOut.views import getSaldo
from .forms import Jadwal
from .models import JadwalForm

def SchedulePost(request):
    if request.method == 'POST' :
        form = Jadwal(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('jadwalIni:SchedulePost')
    else :
        form=Jadwal()
    AllJadwal = JadwalForm.objects.all()
    saldo = getSaldo()
    args={
        'form':form,
        'AllJadwal':AllJadwal,
        'saldo': saldo
    }
    
    return render(request,'schedule.html',args)

        
