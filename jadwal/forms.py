from django import forms
from .models import JadwalForm

class Jadwal(forms.ModelForm) :

    class Meta :
        model = JadwalForm
        fields =['catatan','nominal','tanggal']
        widgets = {
            'tanggal': forms.DateInput(attrs={'type': 'date'})
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control form-horizontal control-label'
             })
            
    
            