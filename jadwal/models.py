from django.db import models

class JadwalForm(models.Model):
    catatan = models.CharField(max_length=150)
    nominal = models.CharField(max_length=300)
    tanggal = models.DateField()

    