from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import WishModel
from InOut.views import getSaldo
from .forms import WishlistModelForm

# Create your views here.
def wishlist_page(request):
    wishes = WishModel.objects.all()
    saldo = getSaldo()
    response = {
            'wishes': wishes,
            'saldo': saldo
    }   
    return render(request, 'wishlist_page.html', response)

def delete_wish(request, id):
    wish = WishModel.objects.filter(id=id)[0]
    saldo = getSaldo()
    response = {
        'wish': wish,
        'saldo': saldo
    }
    if request.method == 'POST':
        wish.delete()
        return redirect('wishlist:wishlist_page')
    return render(request, 'delete_page.html', response)

def delete_all(request):
    wish = WishModel.objects.all()
    if request.method == 'POST':
        wish.delete()
    return redirect('wishlist:wishlist_page')

def add_wishlist(request):
    if request.method == 'POST':
        form = WishlistModelForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
        return redirect("wishlist:wishlist_page")
    else:
        form = WishlistModelForm()
    saldo = getSaldo()
    response = {
        'form': form,
        'saldo': saldo
    }
    return render(request, 'add_wishlist.html', response)