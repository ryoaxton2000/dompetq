from django.test import TestCase, Client
from django.urls import reverse
from wishlist.models import WishModel
from wishlist.forms import WishlistModelForm

# Create your tests here.
class WishlistTest(TestCase):
    def setUp(self):
        self.c = Client()
        self.testCase = WishModel.objects.create(
            nama="test",
            harga="1000"
        )
        self.testId = self.testCase.id

    def test_ada_url_slash_wishlist(self):
        response = self.c.get('/wishlist/')
        self.assertEqual(response.status_code, 200)

    def test_ada_url_slash_wishlist_delete_id(self):
        response = self.c.get(reverse('wishlist:delete', args=[self.testId]))
        self.assertEqual(response.status_code, 200)

    def test_ada_url_slash_wishlist_delete_all(self):
        response = self.c.get('/wishlist/delete-all/')
        self.assertEqual(response.status_code, 302)

    def test_ada_url_slash_make_a_wish(self):
        response = self.c.get('/wishlist/make-a-wish/')
        self.assertEqual(response.status_code, 200)

    def test_wishlist_page_get(self):
        response = self.c.get(reverse('wishlist:wishlist_page'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'wishlist_page.html')
        self.assertTemplateUsed(response, 'base.html')

    def test_delete_wish_get(self):
        response = self.c.get(reverse('wishlist:delete', args=[self.testId]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'delete_page.html')
        self.assertTemplateUsed(response, 'base.html')

    def test_delete_all_get(self):
        response = self.c.get(reverse('wishlist:all_delete'))
        self.assertEqual(response.status_code, 302)

    def test_add_wishlist_get(self):
        form = WishlistModelForm()
        response = self.c.get(reverse('wishlist:add_wishlist'))
        self.assertEqual(response.status_code, 200)
        self.assertFalse(form.is_valid())

    def test_add_wishlist_post(self):
        data_form = {
            'nama': 'test',
            'harga': 10000
        }
        form = WishlistModelForm(data=data_form)
        response = self.c.post(reverse('wishlist:add_wishlist'), {
            'form': form
        })
        self.assertEqual(response.status_code, 302)
        self.assertTrue(form.is_valid())
        self.assertTrue(WishModel.objects.count() > 0)

    def test_delete_wish_post(self):
        response = self.c.post(reverse('wishlist:delete', args=[self.testId]))
        self.assertFalse(response in WishModel.objects.all())
