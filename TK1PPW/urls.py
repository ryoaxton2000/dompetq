from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include("homePage.urls")),
    path('riwayat/', include("InOut.urls")),
    path('promolist/', include("promolist.urls")),
    path('wishlist/', include('wishlist.urls')), 
    path('jadwal/', include('jadwal.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
